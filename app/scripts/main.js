$(document).ready(function() {
    // featured filter start
    $('.featured__filter__buttons li:first-child').addClass('active');
    $('.desc-item').hide();
    $('.desc-item:first').show();

    $('.featured__filter__buttons li').click(function(){
    $('.featured__filter__buttons li').removeClass('active');
    $(this).addClass('active');
    $('.desc-item').hide();
    
    var activeFilter = $(this).find('a').attr('href');
    $(activeFilter).fadeIn();
    return false;
    });
    // featured filter end

    // mobile menu start
    // open 
    $('.menu-button').click(function() {
        $('.menu').css('display', 'block')
        $('.menu__item').removeClass("close-menu")
        $('.menu__item').addClass("open-menu")
        $('body').css('overflow', 'hidden')
    })

    // close
    let menu = $('.menu').get(0)
    window.onclick = function(e) {
        if(e.target == menu) {
            $('.menu__item').addClass("close-menu")
            setTimeout(function() {
                $('body').css('overflow', 'auto')
                $('.menu').css('display', 'none')
                $('.menu__item').removeClass("open-menu")
            }, 400)
        }
    }
    // mobile menu end

})

